import { defineStore } from 'pinia'

export const useAdicionalStore = defineStore({
  id: 'AdicionalStore',
  state: () => ({
    dataRemitente: null,
    dataDestinatario: null,
    dataTasa: null,
    dataDestinatariosClientes: null,
    token: null,
    linkPdf: null
  }),
  actions: {},
  persist: true
})
