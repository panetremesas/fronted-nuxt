import { defineStore } from 'pinia'

export const useTransactionStore = defineStore({
  id: 'TransactionStore',
  state: () => ({
    cliendId: null,
    instrumenId: null,
    rateId: null,
    envio: null,
    recibo: null,
    origin: null,
    destination: null,
  }),
  actions: {},
  persist: true
})
