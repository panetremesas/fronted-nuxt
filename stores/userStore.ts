import { defineStore } from 'pinia'

export const useUserStore = defineStore({
  id: 'UserStore',
  state: () => ({
    id: null,
    sesion: false,
    token: null,
    typeUser: null,
    puller: false,
    name: null,
    sucursalId: null,
    account_owner: false,
    profit_commission: null,
    sucursal_remittances: null,
    sucursal_shipments: null,
    sucursal_name: null,
    sucursal_code: null,
    sucursal_address: null,
    shipments_user: null,
    shipments_password: null
  }),
  actions: {},
  persist: true
})
