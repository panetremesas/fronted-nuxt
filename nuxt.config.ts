// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  ssr: false,
  pages: true,
  devtools: { enabled: true },
  css: ["~/assets/css/main.css"],
  modules: [
    "@ant-design-vue/nuxt",
    "@pinia/nuxt",
    "@pinia-plugin-persistedstate/nuxt",
    "@nuxt/ui",
    "nuxt-primevue"
  ],
  colorMode: {
    preference: 'light'
  },
  ui: {
    icons: ['flagpack']
  },
  nitro: {
    prerender: {
      failOnError: false,
    },
  }
});