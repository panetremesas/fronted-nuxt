import axios from 'axios';

const instanceWTSuramerica = axios.create({
    
});

instanceWTSuramerica.interceptors.request.use(
    async (config) => {
        const userStore = useUserStore();
        if (userStore.token) {
            config.headers.Authorization = `Bearer ${userStore.token}`;
        }
        return config;
    },
    (error) => {
        return Promise.reject(error);
    }
);

export default instanceWTSuramerica;
