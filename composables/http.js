import { ref } from "vue";
import axios from "axios";

// desarrollo
// const baseUrl = "https://produccion.paneteirl.store/api/v1";
const baseUrl = "https://desarrollo.paneteirl.store/api/v1";
// const baseUrl = "http://localhost:4000/api/v1";

const urlLogin = "https://apiapp.suramericacargo.com/api/login";
// const urlLogin = "http://api.test/api/login";
const baseUrlNoAuth = "https://apiapp.suramericacargo.com/api/integraciones";



export function useHttpGet(endpoint) {
  const data = ref(null);
  const error = ref(null);

  try {
    const response = axios.get(`${baseUrl}/${endpoint}`);
    data.value = response.data;
  } catch (err) {
    error.value = err.message;
  }

  return { data, error, fetchData };
}

export function useSurAmericaGet(endpoint) {
  const data = ref(null);
  const error = ref(null);
  async function fetchData() {
    try {
      const response = await axios.get(`${baseUrlNoAuth}/${endpoint}`);
      data.value = response.data;
    } catch (err) {
      error.value = err.message;
    }
  }

  return { data, error, fetchData };
}

export function useSurAmericaPost(endpoint, playload) {
  const data = ref(null);
  const error = ref(null);
  async function postData() {
    try {
      const response = await axios.post(
        `${baseUrlNoAuth}/${endpoint}`,
        playload,
      );
      data.value = response.data;
    } catch (err) {
      error.value = err.message;
    }
  }

  return { data, error, postData };
}

export function useHttpPost(endpoint, payload) {
  const data = ref(false);
  const error = ref(null);

  async function postData() {
    try {
      const response = await axios.post(`${baseUrl}/${endpoint}`, payload);
      data.value = response;
    } catch (err) {
      error.value = err.message;
    }
  }

  return { data, error, postData };
}

export function useLoginSurAmerica(user, password) {
  const data = ref(false);
  const error = ref(null);
  const playload = {
    email: user,
    password: password,
  };

  async function postData() {
    try {
      const response = await axios.post(`${urlLogin}`, playload);
      data.value = response;
    } catch (err) {
      error.value = err.message;
    }
  }

  return { data, error, postData };
}

export function useSurAmericaGenerate(token) {
  const data = ref(null);
  const error = ref(null);
  const guiaStore = useGuiaStore();
  async function guiaData() {
    try {
      const response = await axios.post(`${baseUrlNoAuth}/generar`, guiaStore, {
        headers: {
          Authorization: `Bearer ${token}`,
          Accept: "application/json",
        },
      });
      data.value = response.data;
    } catch (err) {
      error.value = err.message;
    }
  }

  return { data, error, guiaData };
}

export function useHttpGetWithBearer(endpoint) {
  const userStore = useUserStore();

  const data = ref(null);
  const error = ref(null);

  async function fetchData() {
    try {
      const response = await axios.get(`${baseUrl}/${endpoint}`, {
        headers: {
          Authorization: `Bearer ${userStore.token}`,
          Accept: "application/json",
        },
      });
      data.value = response.data;
    } catch (err) {
      error.value = err.message;
    }
  }

  return { data, error, fetchData };
}

export function useHttpPostWithBearer(endpoint, payload) {
  const userStore = useUserStore();
  const data = ref(false);
  const error = ref(null);

  async function postData() {
    try {
      await axios.post(`${baseUrl}/${endpoint}`, payload, {
        headers: {
          Authorization: `Bearer ${userStore.token}`,
          "Content-Type": "application/json",
        },
      });
      data.value = response;
    } catch (err) {
      error.value = err.message;
    }
  }

  return { data, error, postData };
}

export function useHttpPatchWithBearer(endpoint, payload) {
  const userStore = useUserStore();
  const data = ref(false);
  const error = ref(null);

  async function patchData() {
    try {
      await axios.patch(`${baseUrl}/${endpoint}`, payload, {
        headers: {
          Authorization: `Bearer ${userStore.token}`,
          "Content-Type": "application/json",
        },
      });
      data.value = response.data;
    } catch (err) {
      error.value = err.message;
    }
  }

  return { data, error, patchData };
}
