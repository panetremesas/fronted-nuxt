import axios from 'axios';

const instanceWithToken = axios.create({
    // baseURL: 'http://localhost:4000/api/v1/',
    // baseURL: 'https://produccion.paneteirl.store/api/v1',
    baseURL: 'https://desarrollo.paneteirl.store/api/v1',
});

instanceWithToken.interceptors.request.use(
    async (config) => {
        const userStore = useUserStore();
        if (userStore.token) {
            config.headers.Authorization = `Bearer ${userStore.token}`;
        }
        return config;
    },
    (error) => {
        return Promise.reject(error);
    }
);

instanceWithToken.interceptors.response.use(
    (response) => {
        return response;
    },
    async (error) => {
        const userStore = useUserStore();
        if (error.response.status === 401) {
            userStore.sesion = false;
            userStore.token = null;
            userStore.typeUser = null;
            userStore.puller = false;
            userStore.name = null;
            userStore.sucursalId = null;
            userStore.account_owner = false;
            userStore.profit_commission = null;
            userStore.sucursal_remittances = null;
            userStore.sucursal_shipments = null;
            userStore.sucursal_name = null;
            userStore.sucursal_code = null;
            router.push("/");
        }
        return Promise.reject(error);
    }
);

export default instanceWithToken;
